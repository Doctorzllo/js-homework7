let examples = [ "hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv",];
let examples1 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const createLiForItem = (el) => {
    let li = document.createElement('li')
    li.innerHTML = el
    return li
}
const createUlForItems = (el) => {
    let ul = document.createElement('ul')
    ul.append(...el)
    return ul
}

function applyList(array, node) {
    window.onload = () => {
        let lis = array.map((el) => {
            return Array.isArray(el) ? createUlForItems(el.map(createLiForItem)) : createLiForItem(el)
        })

        const parentNode = node || document.body
        parentNode.append(...lis)
    }
}

applyList(examples)
applyList(examples1)